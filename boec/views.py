from django.shortcuts import render
from .models import FighterYear
from .models import Fighter

def boec_list(request):
    fighters_year_list = FighterYear.objects.all().order_by('year')
    return render(request, 'boec_list.html', {'fighters_year_list': fighters_year_list})
