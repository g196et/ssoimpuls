from django.db import models

#Модель отдельного бойца
class Fighter(models.Model):
    name = models.CharField(max_length=300)
    position = models.CharField(max_length=300, default = 'Все должности через запятую')
    about_fighter = models.TextField(default = 'Немного о бойце')
    def publish(self):
        self.save()
    def __str__(self):
        return self.name

#Модель Группы бойцов по годам
class FighterYear(models.Model):
    year = models.CharField(max_length=200)
    #Если убрать эту строку, то все работает
    fighter = models.ForeignKey('Fighter', related_name='test')
    def publish(self):
        self.save()
    def __str__(self):
        return self.year
