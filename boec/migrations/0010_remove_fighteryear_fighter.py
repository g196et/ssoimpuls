# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-02-08 20:44
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('boec', '0009_auto_20160209_0042'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='fighteryear',
            name='fighter',
        ),
    ]
