# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-02-08 20:49
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('boec', '0012_auto_20160209_0048'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fighteryear',
            name='fighter',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='boec.Fighter'),
        ),
    ]
