from django.contrib import admin
from django.db import models
from .models import Fighter
from .models import FighterYear

admin.site.register(FighterYear)
admin.site.register(Fighter)
