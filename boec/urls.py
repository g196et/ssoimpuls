from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.boec_list, name='boec_list'),
]
